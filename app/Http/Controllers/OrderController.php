<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;

class OrderController extends Controller
{
    public function CreateOrder(Request $request)
    {
        $order =  new Order;
        $order->order_user = $request->Name;
        $order->order_food = $request->Makanan;
        $order->order_number = $request->Jumlah;
        $order->save();
        return redirect('/');
    }
    public function ReadOrder(Request $request)
    {
        $reads = Order::All();
        return view('read',compact('reads'));
    }
    public function UpdateOrder($id)
    {
        $order = Order::where('id',$id)->first();
        $order->order_user = "X";
        $order->save();
        return redirect('view');
    }
    public function DeleteOrder($id)
    {
        $order = Order::where('id',$id)->first();
        $order->delete();
        return redirect('view');
    }
    public function Update()
    {
        
        return redirect('update');
    }
}
